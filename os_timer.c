#include "os_timer.h"
#include "os_core.h"

//List of tasks waiting for timeout
os_timer *os_waitlist = NULL;

static void os_waitcallback(void *data, void *data2)
{
	(void)data2; //Suppress warning
	os_tcb *task = (os_tcb*)data;
	os_unblock(task, OS_TIMEOUT);
	os_sched();
}

void os_updatetimers(void)
{
	if(os_waitlist == NULL)
		return;

	os_waitlist->t--;
	while(os_waitlist && !os_waitlist->t)
	{
		os_timer *tim = os_waitlist;
		tim->callback(tim->data, tim->data2);

		//Cancel timer if not already cancelled by callback
		if(tim == os_waitlist)
			os_timercancel(tim);
	}
}

void os_timerenqueue(os_timer *tim)
{
	if(tim == NULL)
		return;

	if(os_waitlist == NULL) //1st item
	{
		os_waitlist = tim;
		tim->next = NULL;
		tim->prev = NULL;
	}
	else if(os_waitlist->t >= tim->t) //New list head
	{
		os_waitlist->t -= tim->t; //List head to delta
		tim->next = os_waitlist;
		tim->prev = NULL;
		os_waitlist->prev = tim;
		os_waitlist = tim;
	}
	else //Insert timer in ascending order
	{
		os_timer *iter = os_waitlist;
		uint32_t accum = os_waitlist->t; //Sum of timeout values

		//Find insertion point
		while(iter->next != NULL && (accum + iter->next->t) <= tim->t)
		{
			accum += iter->next->t;
			iter = iter->next;
		}

		tim->t -= accum; //Absolute to delta
		if(iter->next != NULL) //Not inserting at end of list
		{
			iter->next->prev = tim;
			iter->next->t -= tim->t; //New delta
		}
		tim->next = iter->next;
		tim->prev = iter;
		iter->next = tim;
	}
}

void os_timercancel(os_timer *tim)
{
	if(tim == NULL)
		return;

	if(tim->next)
		tim->next->t += tim->t;

	if(tim == os_waitlist) //Task was list head
		os_waitlist = tim->next;
	if(tim->next != NULL)
		tim->next->prev = tim->prev;
	if(tim->prev != NULL)
		tim->prev->next = tim->next;

	tim->next = NULL;
	tim->prev = NULL;
}

os_error os_suspend(uint32_t t)
{
	if(os_getrunningtask() == NULL) //Not in task context
		return OS_INVCXT;
	if(t == 0)
		return OS_OK;

	os_crit_t c = os_entercritical();

	os_tcb *task = os_getrunningtask();
	os_timer tim;
	tim.t = t;
	tim.data = task;
	tim.data2 = NULL;
	tim.callback = os_waitcallback;
	os_block(WAIT_TIMEOUT, NULL, (t != OS_TIMEOUT_INF) ? &tim : NULL);

	os_exitcritical(c);
	return OS_OK;
}

os_error os_wakeup(os_tcb *task)
{
	os_crit_t c = os_entercritical();
	//Occurs if task is suspended indefinitely using os_suspend
	if((task->timer == NULL) && (task->state == WAIT_TIMEOUT))
	{
		os_unblock(task, OS_TIMEOUT);
		os_sched();
		os_exitcritical(c);
		return OS_OK;
	}
	else
	{
		os_exitcritical(c);
		return OS_INVPARAM;
	}
}
