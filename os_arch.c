#include "os_conf.h"
#include "os_arch.h"
#include "os_core.h"
#define asm __asm__ //Allow code to be built in c99 mode instead of gnu99

//Architecture dependent functions (ARM Cortex-M3/M4/M7)
#define SCB_ICSR (*((volatile uint32_t*)0xE000ED04))
#define SCB_VTOR (*((volatile uint32_t*)0xE000ED08))
#define SCB_CCR  (*((volatile uint32_t*)0xE000ED14))
#ifdef __ARM_FP
#define SCB_CPACR (*((volatile uint32_t*)0xE000ED88))
#define FPU_FPCCR (*((volatile uint32_t*)0xE000EF34))
#endif

#define SYSTICK_CTRL   (*((volatile uint32_t*)0xE000E010))
#define SYSTICK_RELOAD (*((volatile uint32_t*)0xE000E014))
#define SYSTICK_VAL    (*((volatile uint32_t*)0xE000E018))
#define SYSTICK_CALIB  (*((volatile uint32_t*)0xE000E01C))

#define NVIC_PRIO_SYSTICK (*((volatile uint8_t*)0xE000ED23))
#define NVIC_PRIO_PENDSV  (*((volatile uint8_t*)0xE000ED22))

extern os_tcb *os_running;
extern os_tcb *os_nexttask;

void os_cxtswitch(void)
{
	SCB_ICSR |= (1<<28);            //Set PendSV IRQ state to pending
	asm volatile("dsb":::"memory"); //DSB used for SCS access (ARM AN321)
}

void os_cxtswitchisr(void)
{
	SCB_ICSR |= (1<<28);            //Set PendSV IRQ state to pending
	asm volatile("dsb":::"memory"); //DSB used for SCS access (ARM AN321)
}

__attribute__((noreturn))
void os_archstart(void)
{
	//Set interrupt priorities
	NVIC_PRIO_PENDSV = 0xFF;  //PendSV lowest priority
	NVIC_PRIO_SYSTICK = 0xFF; //Systick lowest priority

	//CPU clock source, Enable tick IRQ, Enable timer
	SYSTICK_RELOAD = (OS_CPU_FREQ / OS_TICK_TIMEUS) & 0xFFFFFF;
	SYSTICK_VAL = 0;
	SYSTICK_CTRL |= (1<<2)|(1<<1)|(1<<0);

	//Set exception stack alignment to 8 bytes as required by AAPCS
	SCB_CCR |= (1<<9);

	//PendSV will first switch between the same task
	os_running = os_nexttask;

	//Set PendSV IRQ state to pending
	SCB_ICSR |= (1<<28);

#ifdef __ARM_FP
	//Enable FPU, prevents hardfault if not enabled already
	SCB_CPACR |= (3<<20)|(3<<22);

	//Enable FPU lazy stacking
	FPU_FPCCR |= (1<<30)|(1<<31);

	//Clear FPCA (Floating-Point Context Active) bit
	uint32_t CONTROL;
	asm volatile("mrs %0, control":"=r" (CONTROL));
	CONTROL &= ~(1<<2);
	asm volatile("msr control, %0"::"r" (CONTROL));
#endif

	//Set PSP
	asm volatile("msr psp, %0"::"r" (os_running->SP + 36));

	//Reset MSP, only asm() statements past this point
#ifdef __ARM_ARCH_6M__
	asm volatile("msr msp, %0"::"r" (*((uint32_t*)0))); //No VTOR on M0
#else
	asm volatile("msr msp, %0"::"r" (*((uint32_t*)(SCB_VTOR & 0x1FFFFFFF))));
#endif

	//The +36 above prevents the first tasks software saved register values from being set
#ifdef __ARM_ARCH_6M__
	asm volatile("ldr r4,  =#0x04040404");
	asm volatile("ldr r5,  =#0x05050505");
	asm volatile("ldr r6,  =#0x06060606");
	asm volatile("ldr r7,  =#0x07070707");
	asm volatile("ldr r0,  =#0x08080808");
	asm volatile("mov r8,  r0");
	asm volatile("ldr r0,  =#0x09090909");
	asm volatile("mov r9,  r0");
	asm volatile("ldr r0,  =#0x10101010");
	asm volatile("mov r10, r0");
	asm volatile("ldr r0,  =#0x11111111");
	asm volatile("mov r11, r0");
#else
	asm volatile("mov r4,  #0x04040404");
	asm volatile("mov r5,  #0x05050505");
	asm volatile("mov r6,  #0x06060606");
	asm volatile("mov r7,  #0x07070707");
	asm volatile("mov r8,  #0x08080808");
	asm volatile("mov r9,  #0x09090909");
	asm volatile("mov r10, #0x10101010");
	asm volatile("mov r11, #0x11111111");
#endif

	//Execution starts after interrupts are enabled, in PendSV_Handler()
	asm volatile("cpsie i":::"memory"); //Enable interrupts
	asm volatile("isb");                //ISB so PendSV preempts here (ARM AN321)
	while(1);                           //Never return
}

uintptr_t os_stackinit(void *stack, size_t size, void(*func)(void*), void *arg, uintptr_t *stk_base, size_t *stk_size)
{
	//Align pointer to a 8 byte boundary
	uintptr_t align = ((uintptr_t)stack + size);
	align -= 8;
	align &= 0xFFFFFFF8;
	*stk_base = align;
	*stk_size = align - (uintptr_t)stack + 8; //Account for alignment
	uint32_t *stk = (uint32_t*)align;

	//Hardware saved registers
	*stk-- = 0x01000000;     //xPSR
	*stk-- = (uint32_t)func; //PC
	*stk-- = 0x00000000;     //LR
	*stk-- = 0x12121212;     //R12
	*stk-- = 0x03030303;     //R3
	*stk-- = 0x02020202;     //R2
	*stk-- = 0x01010101;     //R1
	*stk-- = (uint32_t)arg;  //R0 (Task argument pointer)

	//Software saved registers
	*stk-- = 0xFFFFFFFD;     //EXC_RETURN (Use PSP, No FPU)
	*stk-- = 0x11111111;     //R11
	*stk-- = 0x10101010;     //R10
	*stk-- = 0x09090909;     //R9
	*stk-- = 0x08080808;     //R8
	*stk-- = 0x07070707;     //R7
	*stk-- = 0x06060606;     //R6
	*stk-- = 0x05050505;     //R5
	*stk = 0x04040404;       //R4
	return (uintptr_t)stk;
}

void os_wfi(void)
{
	asm volatile("dsb":::"memory"); //DSB needed before WFI/WFE (ARM AN321)
	asm volatile("wfi");
}

uint8_t os_fls(uint8_t v)
{
#ifdef __ARM_ARCH_6M__
	uint8_t ret = 7;
	for(uint8_t mask = 0x80; !(v & mask); mask >>= 1)
		ret--;
	return ret;
#else
	asm volatile("clz %0, %1":"=r" (v):"r" (v));
	return 31 - v;
#endif
}

os_crit_t os_entercritical(void)
{
	os_crit_t ret;
	asm volatile("mrs %0, PRIMASK":"=r" (ret));
	asm volatile("cpsid i":::"memory"); //Nested IRQs, no special action needed
	return ret;
}

void os_exitcritical(os_crit_t c)
{
	asm volatile("msr PRIMASK, %0"::"r" (c));
	asm volatile("isb":::"memory"); //ISB so a pending irq preempts here (ARM AN321)
}

//
// Interrupt Handlers
//
void SysTick_Handler(void)
{
	os_tickhandler();
}

#ifdef __ARM_ARCH_6M__
__attribute__((naked))
void PendSV_Handler(void)
{
	asm volatile("ldr r2, =os_running");  //Load pointer constants
	asm volatile("ldr r3, =os_nexttask"); //
	asm volatile("cpsid i");              //Enter critical section

	asm volatile("mrs r0, PSP");         //Retrieve SP
	asm volatile("sub r0, #36");         //Allocate space for stacked registers
	asm volatile("mov r1, r0");          //Use R1 for stacking
	asm volatile("stmia r1!, {r4-r7}");  //Save regs not saved by HW (Low)
	asm volatile("mov r4, r8");          //Save regs not saved by HW (High)
	asm volatile("mov r5, r9");          //
	asm volatile("mov r6, r10");         //
	asm volatile("mov r7, r11");         //
	asm volatile("stmia r1!, {r4-r7}");  //
	asm volatile("mov r4, lr");          //Return using PSP
	asm volatile("mov r5, #4");          //
	asm volatile("orr r4, r5");          //
	asm volatile("stmia r1!, {r4}");     //Save regs not saved by HW (Link)
	asm volatile("ldr r1, [r2]");        //Dereference os_running
	asm volatile("str r0, [r1]");        //Store SP of running task

	asm volatile("ldr r0, [r3]");        //Dereference os_nexttask
	asm volatile("str r0, [r2]");        //Set os_running = os_nexttask;
	asm volatile("eor r1, r1");          //Set state to RUNNING (0)
	asm volatile("strb r1, [r0, #4]");   //

	asm volatile("ldr r0, [r0]");        //Retrieve SP from ready task
	asm volatile("ldmia r0!, {r4-r7}");  //Load regs not saved by HW (Low)
	asm volatile("ldmia r0!, {r1-r3}");  //Load regs not saved by HW (High)
	asm volatile("mov r8, r1");          //
	asm volatile("mov r9, r2");          //
	asm volatile("mov r10, r3");         //
	asm volatile("ldmia r0!, {r1-r2}");  //Load regs not saved by HW (High+Link)
	asm volatile("mov r11, r1");         //
	asm volatile("mov lr, r2");          //
	asm volatile("msr PSP, r0");         //Set SP

	asm volatile("cpsie i");             //Exit critical section
	asm volatile("bx lr");               //Branch to task
	asm volatile(".ltorg");              //Place literal pool here
}
#else
__attribute__((naked))
void PendSV_Handler(void)
{
	asm volatile("ldr r2, =os_running");              //Load pointer constants
	asm volatile("ldr r3, =os_nexttask");             //
	asm volatile("cpsid i");                          //Enter critical section

	asm volatile("orr lr, #4");                       //Return using PSP
	asm volatile("mrs r0, PSP");                      //Retrieve SP
#ifdef __ARM_FP
	asm volatile("tst lr, #16");                      //Was running task using the FPU?
	asm volatile("it eq");                            //
	asm volatile("vstmdbeq r0!, {s16-s31}");          //Save FP regs not saved by HW
#endif
	asm volatile("stmdb r0!, {r4-r11,lr}");           //Save regs not saved by HW
	asm volatile("ldr r1, [r2]");                     //Dereference os_running
	asm volatile("str r0, [r1]");                     //Store SP of running task

	asm volatile("ldr r0, [r3]");                     //Dereference os_nexttask
	asm volatile("str r0, [r2]");                     //Set os_running = os_nexttask;
	asm volatile("eor r1, r1");                       //Set state to RUNNING (0)
	asm volatile("strb r1, [r0, #4]");                //

	asm volatile("ldr r0, [r0]");                     //Retrieve SP from ready task
	asm volatile("ldmia r0!, {r4-r11,lr}");           //Load regs not saved by HW
#ifdef __ARM_FP
	asm volatile("tst lr, #16");                      //Does new task use the FPU?
	asm volatile("it eq");                            //
	asm volatile("vldmiaeq r0!, {s16-s31}");          //Load FP regs not saved by HW
#endif
	asm volatile("msr PSP, r0");                      //Set SP

	asm volatile("cpsie i");                          //Exit critical section
	asm volatile("bx lr");                            //Branch to task
	asm volatile(".ltorg");                           //Place literal pool here
}
#endif
