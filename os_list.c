#include "os_list.h"

void os_listinit(os_tcblist *list)
{
	list->head = NULL;
	list->tail = NULL;
}

void os_listenqueue(os_tcblist *list, os_tcb *task)
{
	if((list == NULL) || (task == NULL))
		return;

	if((list->head == NULL) && (list->tail == NULL)) //First item
	{
		list->head = task;
		list->tail = task;
		task->next = NULL;
		task->prev = NULL;
	}
	else
	{
		task->next = NULL;
		task->prev = list->tail;
		list->tail->next = task;
		list->tail = task;
	}
}

os_tcb *os_listdequeue(os_tcblist *list)
{
	if((list->head == NULL) && (list->tail == NULL)) //Empty list
		return NULL;

	os_tcb *ret = list->head;

	if(list->head == list->tail) //One item in list
	{
		list->head = NULL;
		list->tail = NULL;
	}
	else
	{
		list->head = list->head->next;
		list->head->prev = NULL;
	}

	ret->next = NULL;
	ret->prev = NULL;
	return ret;
}

int os_listempty(os_tcblist *list)
{
	return ((list->head == NULL) && (list->tail == NULL)) ? 1 : 0;
}

void os_listremovetcb(os_tcblist *list, os_tcb *task)
{
	if((list == NULL) || (task == NULL))
		return;

	if(task == list->head) //Task was list head
		list->head = task->next;
	if(task == list->tail) //Task was list tail
		list->tail = task->prev;
	if(task->next != NULL)
		task->next->prev = task->prev;
	if(task->prev != NULL)
		task->prev->next = task->next;

	task->next = NULL;
	task->prev = NULL;
}
