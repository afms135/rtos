CROSS_COMPILE ?= arm-none-eabi-
CC := $(CROSS_COMPILE)gcc
AR := $(CROSS_COMPILE)ar
DOXY ?= doxygen

BUILD := build
SRCS = os_arch.c os_core.c os_list.c os_prio.c os_timer.c os_mutex.c os_sem.c os_cond.c
LIB = $(BUILD)/libos.a
INCS := $(patsubst %.c,%.h,$(SRCS))
OBJS := $(patsubst %.c,$(BUILD)/%.o,$(SRCS))
DEPS := $(patsubst %.c,$(BUILD)/%.d,$(SRCS))
CCFLAGS += -mcpu=cortex-m3 -mthumb
CCFLAGS += -Os -Wall -Wextra -ffunction-sections -fdata-sections
.PHONY: all clean dir doc

all: dir $(LIB)

$(LIB): $(OBJS)
	@echo 'Creating .ar file'
	$(AR) rcs $@ $^

$(BUILD)/%.o: %.c
	@echo '$< $@'
	@$(CC) $(CCFLAGS) -MMD -MP -c $< -o $@

doc: Doxyfile README.md $(INCS)
	@echo 'Building documention...'
	@$(DOXY) Doxyfile

clean:
	rm -rf $(OBJS) $(DEPS) $(LIB) doc

-include $(DEPS)

dir:
	@mkdir -p $(BUILD)

