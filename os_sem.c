#include "os_sem.h"
#include "os_core.h"
#include "os_list.h"
#include "os_timer.h"

static void os_semtimeout(void *data, void *data2)
{
	os_tcb *task = (os_tcb*)data;
	os_sem *sem = (os_sem*)data2;
	os_listremovetcb(&sem->waiting, task);
	os_unblock(task, OS_TIMEOUT);
	os_sched();
}

void os_seminit(os_sem *s, uint32_t v)
{
	s->v = v;
	os_listinit(&(s->waiting));
}

os_error os_semwait(os_sem *s, uint32_t timeout)
{
	if(os_getrunningtask() == NULL) //Called from ISR
	{
		os_crit_t c = os_entercritical();
		if(s->v == 0) //Can't block in ISR
			return OS_TIMEOUT;
		s->v--;
		os_exitcritical(c);
		return OS_OK;
	}

	os_crit_t c = os_entercritical();
	if(s->v == 0) //Block task
	{
		if(timeout == 0)
		{
			os_exitcritical(c);
			return OS_TIMEOUT;
		}

		os_tcb *wait = os_getrunningtask();
		os_timer timer;
		timer.t = timeout;
		timer.callback = os_semtimeout;
		timer.data = wait;
		timer.data2 = s;
		os_block(WAIT_SEM, &s->waiting, (timeout != OS_TIMEOUT_INF) ? &timer: NULL);
		os_exitcritical(c);

		//Task woken, check if operation timed out
		return wait->ret;
	}
	else
	{
		s->v--;
		os_exitcritical(c);
		return OS_OK;
	}
}

os_error os_semsignal(os_sem *s)
{
	os_crit_t c = os_entercritical();

	if(!os_listempty(&s->waiting)) //Tasks waiting on semaphore
	{
		os_unblock(os_listdequeue(&s->waiting), OS_OK);
		os_sched();
	}
	else
		s->v++;

	os_exitcritical(c);
	return OS_OK;
}
